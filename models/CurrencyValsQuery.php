<?php

namespace yii2portal\currency\models;

/**
 * This is the ActiveQuery class for [[CurrencyVals]].
 *
 * @see CurrencyVals
 */
class CurrencyValsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CurrencyVals[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CurrencyVals|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
