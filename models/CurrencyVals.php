<?php

namespace yii2portal\currency\models;

use Yii;

/**
 * This is the model class for table "currency_vals".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property integer $ord
 * @property integer $is_day
 * @property integer $enabled
 * @property double $rate
 * @property integer $rate_date
 * @property string $dir
 * @property integer $nominal
 * @property string $sred_val
 */
class CurrencyVals extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_vals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['ord', 'is_day', 'enabled', 'rate_date', 'nominal'], 'integer'],
            [['rate'], 'number'],
            [['sred_val'], 'string'],
            [['name', 'title'], 'string', 'max' => 255],
            [['dir'], 'string', 'max' => 5],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'ord' => 'Ord',
            'is_day' => 'Is Day',
            'enabled' => 'Enabled',
            'rate' => 'Rate',
            'rate_date' => 'Rate Date',
            'dir' => 'Dir',
            'nominal' => 'Nominal',
            'sred_val' => 'Sred Val',
        ];
    }

    /**
     * @inheritdoc
     * @return CurrencyValsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CurrencyValsQuery(get_called_class());
    }
}
