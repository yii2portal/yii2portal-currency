<?php


namespace yii2portal\currency\components\informer;

use Yii;
use yii2portal\currency\models\CurrencyVals;
use yii2portal\core\components\Widget;

class Informer extends Widget
{

    public function insert($view)
    {
        $avg = [];
        $rates = CurrencyVals::find()
            ->where(['enabled'=>1])
            ->orderBy(['ord'=>SORT_ASC])
            ->all();

        $timeShift = Yii::$app->getModule('currency')->timeSdv;
        foreach ($rates as $rate) {
            if ($rate->name == 'usd') {
                $avg = unserialize($rate->sred_val);
            }
        }


        return $this->render($view, [
            'rates'=>$rates,
            'timeShift'=>$timeShift,
            'avg'=>(object) $avg
        ]);

    }

}